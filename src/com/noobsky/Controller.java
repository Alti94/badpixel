package com.noobsky;

import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    private static Controller instance;

    @FXML
    private ScrollPane scrollPaneOriginal;

    @FXML
    private ScrollPane scrollPanePreview;

    @FXML
    private Canvas canvasOriginal;

    @FXML
    private Canvas canvasPreview;

    private Mat originalImageMat;
    private Mat previewImageMat;

    private WritableImage previewImage;
    private float originalScale;
    private float previewScale;

    @FXML
    private Label redLabel;
    @FXML
    private Label greenLabel;
    @FXML
    private Label blueLabel;

    @FXML
    private ColorPicker colorPicker;

    @FXML
    private Canvas color;

    private boolean isMatSync = false;


    public static Controller getInstance() {
        return instance;
    }

    public Mat getPreviewImageMat() {
        return previewImageMat;
    }

    @FXML
    private void loadImage() {
        FileChooser fileChooser = new FileChooser();
        ObservableList<FileChooser.ExtensionFilter> extensionFilters =  fileChooser.getExtensionFilters();

        extensionFilters.add(new FileChooser.ExtensionFilter("PNG originalImageMat", "*.png"));
        extensionFilters.add(new FileChooser.ExtensionFilter("JPEG originalImageMat", "*.jpg"));
        extensionFilters.add(new FileChooser.ExtensionFilter("TIFF originalImageMat", "*.tiff"));
        extensionFilters.add(new FileChooser.ExtensionFilter("GIF originalImageMat", "*.gif"));
        extensionFilters.add(new FileChooser.ExtensionFilter("Bitmap originalImageMat", "*.bmp"));

        File file = fileChooser.showOpenDialog(scrollPaneOriginal.getScene().getWindow());
        if (file != null) {
            if (originalImageMat != null) {
                originalImageMat.release();
            }
            if (file.getName().endsWith(".bmp")) {
                originalImageMat = Imgcodecs.imread(file.toString(), Imgcodecs.IMREAD_COLOR);
                postLoad();
            }
            else if (file.getName().endsWith(".gif")) {
                try {
                    BufferedImage bufferedImage = ImageIO.read(file);
                    WritableImage image = SwingFXUtils.toFXImage(bufferedImage, null);

                    PixelReader pixelReader = image.getPixelReader();

                    int width = (int) image.getWidth();
                    int height = (int) image.getHeight();

                    byte[] buffer = new byte[width * height * 4];

                    WritablePixelFormat<ByteBuffer> format = WritablePixelFormat.getByteBgraInstance();
                    pixelReader.getPixels(0, 0, width, height, format, buffer, 0, width * 4);

                    this.originalImageMat = new Mat(height, width, CvType.CV_8UC4);
                    Mat tmp = new Mat();

                    this.originalImageMat.put(0, 0, buffer);

                    Imgproc.cvtColor(this.originalImageMat, tmp, Imgproc.COLOR_BGRA2BGR);
                    this.originalImageMat.release();
                    this.originalImageMat = tmp;

                    postLoad();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else {
                originalImageMat = Imgcodecs.imread(file.toString(), Imgcodecs.IMREAD_COLOR);
                postLoad();
            }
        }

        isMatSync = true;
    }

    private void postLoad() {
        if (previewImageMat != null) {
            previewImageMat.release();
        }

        previewImageMat = originalImageMat.clone();

        originalScale = 1;
        previewScale = 1;

        matToCanvas(0);
        matToCanvas(1);

        SnapshotParameters parameters = new SnapshotParameters();
        parameters.setFill(Color.TRANSPARENT);
        previewImage = canvasPreview.snapshot(parameters, null);
    }

    private void matToCanvas(int side) {

        if (side == 0) {
            canvasOriginal.setWidth(originalImageMat.width());
            canvasOriginal.setHeight(originalImageMat.height());
            canvasOriginal.getGraphicsContext2D().drawImage(ImgUtil.matToImage(originalImageMat), 0, 0);
        }
        else {
            canvasPreview.setWidth(previewImageMat.width());
            canvasPreview.setHeight(previewImageMat.height());
            canvasPreview.getGraphicsContext2D().drawImage(ImgUtil.matToImage(previewImageMat), 0, 0);
        }
    }

    @FXML
    private void saveImage() throws IOException {
        if (originalImageMat != null) {
            Parent root = FXMLLoader.load(getClass().getResource("save_window.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Save image");
            stage.setScene(new Scene(root));
            stage.setResizable(false);
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(scrollPaneOriginal.getScene().getWindow());

            stage.show();
        }
    }

    @FXML
    private void close() {
        ((Stage) scrollPaneOriginal.getScene().getWindow()).close();
    }

    private void refreshOriginal() {
        if (originalScale == 1) {
            matToCanvas(0);
        }
        else {
            Mat resized = new Mat();
            double w = originalImageMat.width() * originalScale;
            double h = originalImageMat.height() * originalScale;
            Imgproc.resize(originalImageMat, resized, new Size(w, h), 0, 0, Imgproc.INTER_NEAREST);

            canvasOriginal.setWidth(resized.width());
            canvasOriginal.setHeight(resized.height());
            canvasOriginal.getGraphicsContext2D().drawImage(ImgUtil.matToImage(resized), 0, 0);
        }
    }

    private void refreshPreview() {
        if (previewScale == 1) {
            matToCanvas(1);
        }
        else {
            Mat resized = new Mat();
            double w = previewImageMat.width() * previewScale;
            double h = previewImageMat.height() * previewScale;
            Imgproc.resize(previewImageMat, resized, new Size(w, h), 0, 0, Imgproc.INTER_NEAREST);

            canvasPreview.setWidth(resized.width());
            canvasPreview.setHeight(resized.height());
            canvasPreview.getGraphicsContext2D().drawImage(ImgUtil.matToImage(resized), 0, 0);
        }
    }

    @FXML
    private void zoomIn(ActionEvent event) {
        String side = ((Button) (event.getSource())).getId();
        if (side.equals("left")) {
            if (originalScale < 8)
                originalScale += 0.125;
            else
                return;

            refreshOriginal();
        }
        else {
            if (previewScale < 8)
                previewScale += 0.125;
            else
                return;

            refreshPreview();
        }
    }

    @FXML
    private void zoomOut(ActionEvent event) {
        String side = ((Button) (event.getSource())).getId();
        if (side.equals("left")) {
            if (originalScale > 0.125)
                originalScale -= 0.125;
            else
                return;

            refreshOriginal();
        }
        else {
            if (previewScale > 0.125)
                previewScale -= 0.125;
            else
                return;

            refreshPreview();
        }
    }

    @FXML
    private void zoomRestore(ActionEvent event) {
        String side = ((Button) (event.getSource())).getId();
        if (side.equals("left")) {
            if (originalScale != 1)
                originalScale = 1;
            else
                return;

            refreshOriginal();
        }
        else {
            if (previewScale != 1)
                previewScale = 1;
            else
                return;

            refreshPreview();
        }
    }

    @FXML
    private void mousePressedEvent(MouseEvent event) {


        Canvas canvas = (Canvas) event.getSource();

        double x = event.getX() / canvas.getWidth();
        double y = event.getY() / canvas.getHeight();

        if (event.isPrimaryButtonDown()) {
            // rysowanie
            if (canvas.getId().equals("right")) {
                int xx = (int) (previewImage.getWidth() * x);
                int yy = (int) (previewImage.getHeight() * y);
                previewImage.getPixelWriter().setColor(xx, yy, colorPicker.getValue());

                //canvasPreview.getGraphicsContext2D().drawImage(previewImage, 0, 0);
                previewImageMat = ImgUtil.imageToMat(previewImage);
                refreshPreview();
            }
        }
        else {
            // pobieranie koloru
            SnapshotParameters parameters = new SnapshotParameters();
            parameters.setFill(Color.TRANSPARENT);
            WritableImage snapshot = canvas.snapshot(parameters, null);

            colorPicker.setValue(snapshot.getPixelReader().getColor((int) event.getX(), (int) event.getY()));
        }
    }

    @FXML
    private void showColorPicker() {
        colorPicker.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        instance = this;

        GraphicsContext gc = color.getGraphicsContext2D();
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, 30, 70);

        colorPicker.valueProperty().addListener((observable, oldValue, newValue) -> {
            int red = (int) (newValue.getRed() * 255);
            redLabel.setText(String.valueOf(red));

            int green = (int) (newValue.getGreen() * 255);
            greenLabel.setText(String.valueOf(green));

            int blue = (int) (newValue.getBlue() * 255);
            blueLabel.setText(String.valueOf(blue));

            GraphicsContext graphicsContext = color.getGraphicsContext2D();
            graphicsContext.setFill(newValue);
            graphicsContext.fillRect(0, 0, 30, 70);
        });

        scrollPaneOriginal.vvalueProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.doubleValue() != newValue.doubleValue())
                scrollPanePreview.setVvalue(newValue.doubleValue());
        });

        scrollPaneOriginal.hvalueProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.doubleValue() != newValue.doubleValue())
                scrollPanePreview.setHvalue(newValue.doubleValue());
        });

        scrollPanePreview.vvalueProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.doubleValue() != newValue.doubleValue())
                scrollPaneOriginal.setVvalue(newValue.doubleValue());
        });

        scrollPanePreview.hvalueProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.doubleValue() != newValue.doubleValue())
                scrollPaneOriginal.setHvalue(newValue.doubleValue());
        });


    }
}
