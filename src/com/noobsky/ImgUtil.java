package com.noobsky;

import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritablePixelFormat;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;

import java.io.ByteArrayInputStream;
import java.nio.ByteBuffer;

/**
 * Created by kamil on 14.03.17.
 */
public class ImgUtil {
    public static Mat imageToMat(Image src) {
        PixelReader pixelReader = src.getPixelReader();

        int width = (int) src.getWidth();
        int height = (int) src.getHeight();
        byte[] buffer = new byte[width * height * 4];

        WritablePixelFormat<ByteBuffer> format = WritablePixelFormat.getByteBgraInstance();
        pixelReader.getPixels(0, 0, width, height, format, buffer, 0, width * 4);

        Mat dst = new Mat(height, width, CvType.CV_8UC4);
        dst.put(0, 0, buffer);

        return dst;
    }

    public static Image matToImage(Mat src) {
        MatOfByte matOfByte = new MatOfByte();
        Imgcodecs.imencode(".png", src, matOfByte);

        Image dst = new Image(new ByteArrayInputStream(matOfByte.toArray()));
        matOfByte.release();

        return dst;
    }
}
